package com.cihangir;

import com.cihangir.domain.Address;
import com.cihangir.domain.Hotel;
import com.cihangir.domain.QHotel;
import com.cihangir.domain.Review;
import com.cihangir.repository.HotelRepository;
import com.querydsl.core.types.dsl.BooleanExpression;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ApplicationTests {

    @Autowired
    HotelRepository hotelRepository;


    @Test
    public void contextLoads() {
    }


    @Test
    public void testAddNewHotel() {
        Hotel h1 = new Hotel(
                "Cihangir",
                130,
                new Address("Samsun", "Turkey"),
                Arrays.asList(
                        new Review("Bilal",8, false),
                        new Review("Metehan",9,true)
                )
        );


        Hotel h2 = new Hotel(
                "Cavusoglu",
                90,
                new Address("Istanbul", "Turkey"),
                Arrays.asList(
                        new Review("Mahmut",9, true)
                )
        );

        Hotel h3 = new Hotel(
                "Selimiye",
                120,
                new Address("London", "England"),
                new ArrayList<>()
        );

        this.hotelRepository.deleteAll();

        List<Hotel> hotels = Arrays.asList(h1,h2,h3);
        this.hotelRepository.saveAll(hotels);
    }

    @Test
    public void testFindHotelByCountry() {

        String country = "England";

        QHotel qHotel = new QHotel("hotel");
        BooleanExpression filterByCountry = qHotel.address.country.eq(country);


        List<Hotel> hotels = (List<Hotel>) this.hotelRepository.findAll(filterByCountry);

        hotels.forEach( el-> {
            System.out.println(el.getName() + "-" + el.getPricePerNight());
        });



    }









}
