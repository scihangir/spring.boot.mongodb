package com.cihangir.resource;

import com.cihangir.domain.Hotel;
import com.cihangir.domain.QHotel;
import com.cihangir.repository.HotelRepository;
import com.querydsl.core.types.dsl.BooleanExpression;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/hotels")
public class HotelResource {

    private final HotelRepository hotelRepository;

    public HotelResource(HotelRepository hotelRepository) {
        this.hotelRepository = hotelRepository;
    }


    @GetMapping
    public List<Hotel> getAll() {
        return this.hotelRepository.findAll();
    }

    @GetMapping("/{country}")
    public List<Hotel> findByCountry(@PathVariable String country) {

        QHotel qHotel = new QHotel("hotel");
        BooleanExpression filterByCountry = qHotel.address.country.eq(country);


        List<Hotel> hotels = (List<Hotel>) this.hotelRepository.findAll(filterByCountry);
        return hotels;
    }

    @GetMapping("/recommend")
    public List<Hotel> getRecommended() {

        final int maxPrice = 100;
        final int minRating = 7;


        QHotel qHotel = new QHotel("hotel");

        BooleanExpression filterByPrice = qHotel.pricePerNight.lt(maxPrice);
        BooleanExpression filterByRating = qHotel.reviews.any().rating.gt(minRating);

        List<Hotel> hotels = (List<Hotel>) this.hotelRepository.findAll(filterByPrice.and(filterByRating));
        return hotels;
    }


}
